# ansible-role-node_exporter
Ansible Role to install the node_exporter binary

## How to install
### requirements.yml
**Put the file in your roles directory**
```yaml
---
- src: https://gitlab.com/adieperi/ansible-role-node_exporter.git
  scm: git
  version: main
  name: ansible-role-ansible-role-node_exporter
```
### Download the role
```Shell
ansible-galaxy install -f -r ./roles/requirements.yml --roles-path=./roles
```

## Requirements

- Ansible >= 2.10 **(No tests has been realized before this version)**

## Role Variables

All variables which can be overridden are stored in [default/main.yml](default/main.yml) file as well as in table below.

| Name           | Default Value | Choices | Description                        |
| -------------- | ------------- | ------- | -----------------------------------|
| `node_exporter_version` | latest | [version](https://github.com/prometheus/node_exporter/tags) | node_exporter version. |
| `node_exporter_force_upgrade` | false | true / false | Enable or disable upgrade option. |
| `node_exporter_log_level` | info | debug, info, warn, error | Only log messages with the given severity or above. |
| `node_exporter_log_enabled` | true | true / false | Enable or disable logging. |
| `node_exporter_log_format` | logfmt | logfmt, json | Output format of log messages. |
| `node_exporter_listen_addresses` | [] |  | Addresses on which to expose metrics and web interface. |
| `node_exporter_extra_opts` | [] |  | Define node_exporter extra options at start. |
| `node_exporter_web_configuration` | [] |  | Variable that contain web configurations. |

## Example Playbook

```yaml
---
- hosts: all
  tasks:
    - name: Include ansible-role-node_exporter
      include_role:
        name: ansible-role-node_exporter
      vars:
        node_exporter_version: '1.4.0'
        node_exporter_web_configuration:
          basic_auth_users:
            prometheus: $2b$12$hNf2lSsxfm0.i4a.1kVpSOVyBCfIB51VRjgBUyv6kdnyTlgWj81Ay
        node_exporter_extra_opts:
          - collector.ethtool
          - collector.textfile.directory='/tmp'
```
## License

This project is licensed under MIT License. See [LICENSE](/LICENSE) for more details.

## Maintainers and Contributors

- [Anthony Dieperink](https://gitlab.com/adieperi)
